import folium
import pandas

map = folium.Map(location=[38.58,-99.09], zoom_start=6, tiles="Mapbox Bright")
data=pandas.read_csv("Volcanoes.txt")
lat=list(data["LAT"])
lon=list(data["LON"])
elev = list(data["ELEV"])

def color_producer(elevation):
	if elevation < 1000:
		return 'green'
	elif 1000 <= elevation <= 3000:
		return 'blue'
	else:
		return 'red'

fgv=folium.FeatureGroup(name="volcanoes")
fgp=folium.FeatureGroup(name="population")

for lt,ln,el in zip(lat,lon,elev):
	fgv.add_child(folium.CircleMarker(location=[lt,ln], radius=7, popup=str(el)+" m",
	fill_color=color_producer(el), color='grey', fill_opacity=0.5))

fgp.add_child(folium.GeoJson(data=open('world.json','r', encoding='utf-8-sig'),
style_function=lambda x: {'fillColor':'yellow' if x['properties']['POP2005'] < 100000000
else 'green' if 100000000 <= x['properties']['POP2005'] < 300000000 
else 'red' }))


map.add_child(fgv)
map.add_child(fgp)
map.add_child(folium.LayerControl())
map.save("Map1.html")