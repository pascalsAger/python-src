import json
from difflib import get_close_matches
data=json.load(open("data.json"))
dict_keys=data.keys()



def translate(word):
	if word in data:
		return data[word]
	elif len(get_close_matches(word,dict_keys,cutoff=0.8)) > 0:
		cont=input("Did you mean %s instead? Enter Y if yes, or N if no. " %get_close_matches(word,dict_keys)[0])
		if cont=="Y" or cont == 'y':
			return data[get_close_matches(word,dict_keys)[0]]
		elif cont=="N" or cont == 'n':
			return "The word doesn't exist. Please double check!"
		else:
			return "We didn't understand your entry."
	else:
		return "The word doesn't exist. Please double check!"


word=input("Enter Word: ")
output=translate(word.lower())

if type(output) == list:
	for item in output:
		print(item)
else:
	print(output)