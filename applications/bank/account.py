class Account:
    def __init__(self,filepath):
    	self.store = filepath
    	with open(filepath,'r') as file:
    		self.balance=int(file.read())

    def withdraw(self, amount):
    	if amount <= self.balance:
    		self.balance=self.balance-amount
    		with open(self.store,'w') as file:
    			file.write(str(self.balance))  	

    	else:
    	    raise Exception('Insufficient balance!!!')


    def deposit(self, amount):
    	self.balance=self.balance+amount
    	with open(self.store,'w') as file:
    		file.write(str(self.balance))  	
	    

class Checking(Account):
	'''This calss generates checking account objects'''
	type="Checking"
	def __init__(self,filepath, fee):
		Account.__init__(self,filepath)
		self.fee=fee

	def transfer(self,amount):
		if amount+self.fee <= self.balance:
			self.balance=self.balance-amount-self.fee
			with open(self.store,'w') as file:
				file.write(str(self.balance))  
		else:
			raise Exception('Insufficient funds!!!')		


checking=Checking("balance.txt",10)
checking.deposit(100)
checking.transfer(50)
print(checking.balance)
print(checking.type)
print(checking.__doc__)