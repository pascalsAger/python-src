from flask import Flask, render_template, redirect, url_for, flash
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import InputRequired, Email, Length, EqualTo
from werkzeug.security import generate_password_hash, check_password_hash
import smtplib
from email.mime.text import MIMEText

app = Flask(__name__)
Bootstrap(app)

#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:test123@localhost/postgress'

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://uuxwivujmdecmn:7d0430288677fc76224192e29b2045ca72de774e06a1b938bcdff62c013350ac@ec2-23-23-225-12.compute-1.amazonaws.com:5432/d9nii7tfdodang?sslmode=require'
app.config['SECRET_KEY'] = 'thisissecret'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'


def send_email(email,name):
	FROM_EMAIL="advith.arsenal@gmail.com"
	FROM_PASSWORD="aldoushuxley"
	message="Hello "+name+",<br> \
	Thank you for registering with Pascals incorporated. Please feel free to use out free API's and enjoy building insights.  \
	If you are keen on a premium account, hit us up and we'll discuss your options. <br> \
	Thank you <br> \
	<b> Pascals Inc </b>"

	print("e: ",email)
	print("m: ",message)

	subject="Pascals Inc - Registrations Confirmation"
	toList=[email]

	gmail = smtplib.SMTP('smtp.gmail.com',587)
	gmail.ehlo()
	gmail.starttls()
	gmail.login(FROM_EMAIL,FROM_PASSWORD)

	msg=MIMETEXT(message, 'html')
	msg['Subject']=subject
	msg['To']=','.join(toList)
	msg['From']=FROM_EMAIL
	gmail.send_message(msg)	


class User(UserMixin, db.Model):
	__tablename__ = "users"
	id = db.Column(db.Integer, primary_key=True)
	username_ = db.Column(db.String(15), unique=True)
	email_ = db.Column(db.String(50), unique=True)
	password_ = db.Column(db.String(80))

@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))	

class LoginForm(FlaskForm):
	username = StringField('Username', validators=[InputRequired(), Length(min=4, max=15)])
	password = PasswordField('Password', validators=[InputRequired(), Length(min=8, max=80)])
	remember = BooleanField('Remember me')


class RegisterForm(FlaskForm):
	email = StringField('Email', validators=[InputRequired(), Email(message='Invalid email'), Length(max=50)])
	username = StringField('Username', validators=[InputRequired(), Length(min=4, max=15)])
	password = PasswordField('Password', validators=[InputRequired(), Length(min=8, max=80), EqualTo('confirm_password', message='Passwords do not match!')])
	confirm_password = PasswordField('Confirm Password')
	


@app.route('/')
def index():
	return render_template('index.html')


@app.route('/dashboard')
@login_required
def dashboard():
	return render_template('dashboard.html', name=current_user.username_)

@app.route('/contact')
def contact():
	return render_template('contact.html')	


@app.route('/login', methods=['GET','POST'])
def login():
	login_form = LoginForm()
	if login_form.validate_on_submit():
		user = User.query.filter_by(username_=login_form.username.data).first()
		if user:
			if check_password_hash(user.password_,login_form.password.data):
				login_user(user, remember=login_form.remember.data)
				return redirect(url_for('dashboard'))
			else:
				flash("Incorrect Password")
				return redirect(url_for('login'))
		else:
			flash("Incorrect Username")
			return redirect(url_for('login'))		
		

	return render_template('login.html', login_form=login_form, signup_form=RegisterForm(), page='login_page')

@app.route('/signup', methods=['GET','POST'])
def signup():
	signup_form = RegisterForm()

	if signup_form.validate_on_submit():
		hashed_password = generate_password_hash(signup_form.password.data, method='sha256')
		new_user = User(username_=signup_form.username.data, email_=signup_form.email.data, password_=hashed_password)
		db.session.add(new_user)
		db.session.commit()
		#send_email(new_user.email_,new_user.username_)
		return render_template('login.html', login_form=LoginForm(), signup_form=signup_form, page='login_page')
	else:
		return render_template('login.html', login_form=LoginForm(), signup_form=signup_form, page='signup_page')
	



@app.route('/logout')
@login_required
def logout():
	logout_user()
	return redirect(url_for('index'))

if __name__ == '__main__':
	app.debug=True
	app.run()

