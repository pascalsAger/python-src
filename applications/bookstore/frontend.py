from tkinter import *
import backend

def get_selected_row(event):
	global selected_tuple
	index=list1.curselection()[0]
	selected_tuple=list1.get(index)
	e1.delete(0,END)
	e1.insert(END,selected_tuple[1])
	e2.delete(0,END)
	e2.insert(END,selected_tuple[2])
	e3.delete(0,END)
	e3.insert(END,selected_tuple[3])
	e4.delete(0,END)
	e4.insert(END,selected_tuple[4])		

def view_command():
	list1.delete(0,END)
	for row in backend.view():
		list1.insert(END,row)


def search_command():
	list1.delete(0,END)
	for row in backend.search(title_value.get(),author_value.get(),year_value.get(),isbn_value.get()):
		list1.insert(END,row)

def add_command():
	backend.insert(title_value.get(),author_value.get(),year_value.get(),isbn_value.get())
	list1.delete(0,END)
	list1.insert(END,(title_value.get(),author_value.get(),year_value.get(),isbn_value.get()))

def update_command():
	backend.update(selected_tuple[0],title_value.get(),author_value.get(),year_value.get(),isbn_value.get())

def del_command():
	backend.delete(selected_tuple[0])	

window=Tk()

l1=Label(window,text="Title")
l1.grid(row=0,column=0)


title_value=StringVar()
e1=Entry(window,textvariable=title_value)    #create a window for capturing user data
e1.grid(row=0,column=1)



l2=Label(window,text="Author")
l2.grid(row=0,column=2)

author_value=StringVar()
e2=Entry(window,textvariable=author_value)    #create a window for capturing user data
e2.grid(row=0,column=3)


l2=Label(window,text="Year")
l2.grid(row=1,column=0)

year_value=IntVar()
e3=Entry(window,textvariable=year_value)    #create a window for capturing user data
e3.grid(row=1,column=1)

l2=Label(window,text="ISBN")
l2.grid(row=1,column=2)

isbn_value=StringVar()
e4=Entry(window,textvariable=isbn_value)    #create a window for capturing user data
e4.grid(row=1,column=3)



b1=Button(window,text="View",width=10,command=view_command) #create a button widget, associate it with a function
b1.grid(row=2,column=3)   #place it in the grid

b2=Button(window,text="Search Entry",width=10,command=search_command) #create a button widget, associate it with a function
b2.grid(row=3,column=3)   #place it in the grid

b3=Button(window,text="Add Entry",width=10,command=add_command) #create a button widget, associate it with a function
b3.grid(row=4,column=3)   #place it in the grid

b4=Button(window,text="Update",width=10,command=update_command) #create a button widget, associate it with a function
b4.grid(row=5,column=3)   #place it in the grid

b5=Button(window,text="Delete",width=10,command=del_command) #create a button widget, associate it with a function
b5.grid(row=6,column=3)   #place it in the grid

b6=Button(window,text="Close",width=10,command=window.destroy) #create a button widget, associate it with a function
b6.grid(row=7,column=3)   #place it in the grid


s1=Scrollbar(window,width=14)
s1.grid(row=2,column=2,rowspan=6,columnspan=1, sticky='ns')


list1=Listbox(window, height=10,width=30)
list1.grid(row=2,column=0, rowspan=6, columnspan=2)

list1.bind('<<ListboxSelect>>',get_selected_row)

list1.configure(yscrollcommand=s1.set)
s1.configure(command=list1.yview)

window.mainloop()