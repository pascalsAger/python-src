import psycopg2

#connect to a database
#create a cursor object
#write an SQL query
#commit changes
def create_table():
	conn=psycopg2.connect("dbname='postgress' user='postgres' password='test123' host='localhost' port='5432'")
	cur=conn.cursor()
	cur.execute("CREATE TABLE IF NOT EXISTS book_store (id SERIAL PRIMARY KEY, title TEXT, author TEXT, year INTEGER, isbn TEXT)")
	conn.commit()
	conn.close()



def insert(title,author,year,isbn):
	conn=psycopg2.connect("dbname='postgress' user='postgres' password='test123' host='localhost' port='5432'")
	cur=conn.cursor()
	cur.execute("INSERT INTO book_store VALUES(DEFAULT,%s,%s,%s,%s)",(title,author,year,isbn))
	conn.commit()
	conn.close()

def view():
	conn=psycopg2.connect("dbname='postgress' user='postgres' password='test123' host='localhost' port='5432'")
	cur=conn.cursor()
	cur.execute("SELECT * FROM book_store")
	rows=cur.fetchall()
	conn.close()
	return rows

def delete(id):
	conn=psycopg2.connect("dbname='postgress' user='postgres' password='test123' host='localhost' port='5432'")
	cur=conn.cursor()
	cur.execute("DELETE FROM book_store WHERE id=%s",(id,))
	conn.commit()
	conn.close()

def search(title=None,author=None,year=None,isbn=None):
	conn=psycopg2.connect("dbname='postgress' user='postgres' password='test123' host='localhost' port='5432'")
	cur=conn.cursor()
	print(author)
	print(year)
	cur.execute("SELECT * FROM  book_store WHERE title=%s OR author=%s OR year=%s OR isbn=%s",(title,author,year,isbn))
	rows=cur.fetchall()
	conn.commit()
	conn.close()
	return rows

def update(id,title,author,year,isbn):
	conn=psycopg2.connect("dbname='postgress' user='postgres' password='test123' host='localhost' port='5432'")
	cur=conn.cursor()
	cur.execute("UPDATE book_store SET title=%s, author=%s, year=%s, isbn=%s  WHERE id=%s",(title,author,year,isbn,id))
	conn.commit()
	conn.close()	

'''insert("Advith","MOCHA",1876,"465498")
print(view())'''

#print(search(author='TEST'))

#print(__name__)


create_table()
#insert("Advith","MOCHA",1876,"465498")