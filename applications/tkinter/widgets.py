from tkinter import *

window = Tk()

def convert():
	grams=str(float(e1_value.get())*1000) + " grams" 
	pounds=str(float(e1_value.get())*2.20462) + " lbs"
	ounces=str(float(e1_value.get())*35.274) + " ounces"
	t1.insert(END,grams)
	t2.insert(END,pounds)
	t3.insert(END,ounces)



l1 = Label(window, text="Kilograms")
l1.grid(row=0,column=0)



e1_value=StringVar()
e1=Entry(window,textvariable=e1_value)    #create a window for capturing user data
e1.grid(row=0,column=1)

b1=Button(window,text="convert",command=convert) #create a button widget, associate it with a function
b1.grid(row=0,column=2)   #place it in the grid


t1=Text(window,height=1,width=20)
t1.grid(row=1,column=0)

t2=Text(window,height=1,width=20)
t2.grid(row=1,column=1)

t3=Text(window,height=1,width=20)
t3.grid(row=1,column=2)


window.mainloop()