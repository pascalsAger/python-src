from tkinter import *


window=Tk()
#create window and widgets between the above line and window.mainloop()
#all the gui objects are created after this.

def km_to_miles():
	miles=float(e1_value.get())*1.6
	t1.insert(END,miles)

b1=Button(window,text="Execute",command=km_to_miles) #create a button widget, associate it with a function
b1.grid(row=0,column=0)   #place it in the grid

e1_value=StringVar()
e1=Entry(window,textvariable=e1_value)    #create a window for capturing user data
e1.grid(row=0,column=1)


t1=Text(window,height=1,width=20)
t1.grid(row=0,column=2)

window.mainloop() #this statement always comes at the end