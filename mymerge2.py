import datetime
import glob2

filename=datetime.datetime.now()
srcfiles = glob2.glob("file*.txt")

def merge2_file():
	with open(filename.strftime("%Y-%m-%d-%H-%M-%S-%f")+".txt","w+") as file:
		for i in srcfiles:
			with open(i,"r") as src:
				text=src.read()
				file.write(text+"\n")
merge2_file()