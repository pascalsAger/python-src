import psycopg2


class Database:
	#connect to a database
	#create a cursor object
	#write an SQL query
	#commit changes
	def __init__(self):
		self.conn=psycopg2.connect("dbname='postgress' user='postgres' password='test123' host='localhost' port='5432'")
		self.cur=self.conn.cursor()
		self.cur.execute("CREATE TABLE IF NOT EXISTS book_store (id SERIAL PRIMARY KEY, title TEXT, author TEXT, year INTEGER, isbn TEXT)")
		self.conn.commit()
		



	def insert(self,title,author,year,isbn):
		self.cur.execute("INSERT INTO book_store VALUES(DEFAULT,%s,%s,%s,%s)",(title,author,year,isbn))
		self.conn.commit()
		

	def view(self):
		self.cur.execute("SELECT * FROM book_store")
		rows=self.cur.fetchall()
		return rows

	def delete(self,id):
		self.cur.execute("DELETE FROM book_store WHERE id=%s",(id,))
		self.conn.commit()
		

	def search(self,title=None,author=None,year=None,isbn=None):
		self.cur.execute("SELECT * FROM  book_store WHERE title=%s OR author=%s OR year=%s OR isbn=%s",(title,author,year,isbn))
		rows=self.cur.fetchall()
		self.conn.commit()
		return rows

	def update(self,id,title,author,year,isbn):
		self.cur.execute("UPDATE book_store SET title=%s, author=%s, year=%s, isbn=%s  WHERE id=%s",(title,author,year,isbn,id))
		self.conn.commit()

	def __del__(self):
		self.conn.close()
		

	'''insert("Advith","MOCHA",1876,"465498")
	print(view())'''

	#print(search(author='TEST'))

	#print(__name__)


	#create_table()
	#insert("Advith","MOCHA",1876,"465498")