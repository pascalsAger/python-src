from tkinter import *
from backend import Database

class Bookstore:

	def __init__(self):
		self.backend=Database()
		self.window=Tk()

		self.l1=Label(self.window,text="Title")
		self.l1.grid(row=0,column=0)


		self.title_value=StringVar()
		self.e1=Entry(self.window,textvariable=self.title_value)    #create a window for capturing user data
		self.e1.grid(row=0,column=1)



		self.l2=Label(self.window,text="Author")
		self.l2.grid(row=0,column=2)

		self.author_value=StringVar()
		self.e2=Entry(self.window,textvariable=self.author_value)    #create a window for capturing user data
		self.e2.grid(row=0,column=3)


		self.l2=Label(self.window,text="Year")
		self.l2.grid(row=1,column=0)

		self.year_value=IntVar()
		self.e3=Entry(self.window,textvariable=self.year_value)    #create a window for capturing user data
		self.e3.grid(row=1,column=1)

		self.l2=Label(self.window,text="ISBN")
		self.l2.grid(row=1,column=2)

		self.isbn_value=StringVar()
		self.e4=Entry(self.window,textvariable=self.isbn_value)    #create a window for capturing user data
		self.e4.grid(row=1,column=3)



		self.b1=Button(self.window,text="View",width=10,command=self.view_command) #create a button widget, associate it with a function
		self.b1.grid(row=2,column=3)   #place it in the grid

		self.b2=Button(self.window,text="Search Entry",width=10,command=self.search_command) #create a button widget, associate it with a function
		self.b2.grid(row=3,column=3)   #place it in the grid

		self.b3=Button(self.window,text="Add Entry",width=10,command=self.add_command) #create a button widget, associate it with a function
		self.b3.grid(row=4,column=3)   #place it in the grid

		self.b4=Button(self.window,text="Update",width=10,command=self.update_command) #create a button widget, associate it with a function
		self.b4.grid(row=5,column=3)   #place it in the grid

		self.b5=Button(self.window,text="Delete",width=10,command=self.del_command) #create a button widget, associate it with a function
		self.b5.grid(row=6,column=3)   #place it in the grid

		self.b6=Button(self.window,text="Close",width=10,command=self.window.destroy) #create a button widget, associate it with a function
		self.b6.grid(row=7,column=3)   #place it in the grid


		self.s1=Scrollbar(self.window,width=14)
		self.s1.grid(row=2,column=2,rowspan=6,columnspan=1, sticky='ns')


		self.list1=Listbox(self.window, height=10,width=30)
		self.list1.grid(row=2,column=0, rowspan=6, columnspan=2)

		self.list1.bind('<<ListboxSelect>>',self.get_selected_row)

		self.list1.configure(yscrollcommand=self.s1.set)
		self.s1.configure(command=self.list1.yview)
		self.window.mainloop()



	def get_selected_row(self,event):
#		global selected_tuple
		index=self.list1.curselection()[0]
		self.selected_tuple=list1.get(index)
		self.e1.delete(0,END)
		self.e1.insert(END,selected_tuple[1])
		self.e2.delete(0,END)
		self.e2.insert(END,selected_tuple[2])
		self.e3.delete(0,END)
		self.e3.insert(END,selected_tuple[3])
		self.e4.delete(0,END)
		self.e4.insert(END,selected_tuple[4])		

	def view_command(self):
		self.list1.delete(0,END)
		for row in self.backend.view():
			self.list1.insert(END,row)


	def search_command(self):
		self.list1.delete(0,END)
		for row in self.backend.search(self.title_value.get(),self.author_value.get(),self.year_value.get(),self.isbn_value.get()):
			self.list1.insert(END,row)

	def add_command():
		self.backend.insert(self.title_value.get(),self.author_value.get(),self.year_value.get(),self.isbn_value.get())
		self.list1.delete(0,END)
		self.list1.insert(END,(self.title_value.get(),self.author_value.get(),self.year_value.get(),self.isbn_value.get()))

	def update_command(self):
		self.backend.update(self.selected_tuple[0],self.title_value.get(),self.author_value.get(),self.year_value.get(),self.isbn_value.get())

	def del_command(self):
		self.backend.delete(self.selected_tuple[0])	

		

if __name__ == '__main__':
			book=Bookstore()	