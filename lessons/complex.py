class Complex:
	def __init__(self,real,img):
		self.real=real
		self.img=img

	def __repr__(self):
		return str(self.real)+' + i'+str(self.img)

	def __get__(self,real):
		return self.real	