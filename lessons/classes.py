class Dog:
	def __init__(self,name):
		self.name = name
		self.tricks = []

	def add_tricks(self,trick):
		self.tricks.append(trick)
'''

d = Dog('Fido')
e = Dog('Buddy')
d.add_tricks('Roll over')
e.add_tricks('Play Dead')

print(d.tricks)
print(e.tricks)
'''