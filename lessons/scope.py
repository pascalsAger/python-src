def scope_test():
	def do_local():
		spam="local spam"

	def do_nonlocal():
		nonlocal spam
		spam="nonlocal spam"	

	def do_global():
		global spam
		spam="global spam"

	spam="test spam"
	print("After local assignment", spam)
	do_local()
	print("After do_local assignment", spam)
	do_nonlocal()
	print("After do_nonlocal assignment", spam)
	do_global()
	print("After do_global assignment", spam)

scope_test()
print("In global scope", spam)