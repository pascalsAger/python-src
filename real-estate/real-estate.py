import sys
import requests
from bs4 import BeautifulSoup
import re
import pandas
import folium

def scrape():
    lst=[]
    for i in range(1,6):
        url="http://www.century21global.com/for-sale-residential/Germany/Berlin?pageNo="+str(i)
        try:
            res=requests.get(url)
        except requests.exceptions.RequestException as e:
            print(e)
            sys.exit(1)
        #res=requests.get(url)
        contents=res.content
        soup=BeautifulSoup(contents,"html.parser")
        res =soup.findAll('div',class_=re.compile("search-result row"))
        for result in res:
            d={}
            try:
                input=result.find('input',class_="map-coordinates")
                #print(input['data-lat'])
                #print(input['data-lng'])
                d["Latitude"]=input['data-lat']
                d["Longitude"]=input['data-lng']
            except:
                d["Latitude"]=str(52.5200)
                d["Longitude"]=str(13.4050)
            #print(input)
            #for inp in input:
            #a=result.find('a',class_=re.compile("search-result-info"))

            try:
                price=result.find("div",class_="search-result-label-primary price-native")
                d["Price"]=price.getText().strip()
            except:
                d["Price"]=str(0)

            try:
                city=result.find("span",class_="property-address")
                d["City"]=city.getText().strip()
            except:
                d["City"]='Berlin'

            try:
                size=result.find("div",class_="size")
                d["Size"]=size.getText().strip()
            except:
                d["Size"]="0 sq. m"

            lst.append(d)
    return lst        


def color_producer(price):
    if price < 200000:
        return 'green'
    elif 200000 <= price <= 400000:
        return 'blue'
    elif 400000 <= price <= 700000:
        return 'orange'    
    else:
        return 'red'


#df.to_csv("Output.csv")
#df.to_json("Output.json")

def __main__():
    lst=scrape()
    data=pandas.DataFrame(lst)
    data['Price']=data['Price'].replace('[\€,EUR]','',regex=True).astype(float)
    map = folium.Map(location=[52.5200,13.4050], zoom_start=10, tiles="Mapbox Bright")
    lat=list(data["Latitude"])
    lon=list(data["Longitude"])
    price = list(data["Price"])

    fgr=folium.FeatureGroup(name="Berlin real estate")

    for lt,ln,pr in zip(lat,lon,price):
        fgr.add_child(folium.CircleMarker(location=[lt,ln], radius=7, popup=str(pr)+" €",
        fill_color=color_producer(pr), color='grey', fill_opacity=0.5))

    map.add_child(fgr) 
    map.save("Map1.html")   



if __name__=="__main__":
    __main__()