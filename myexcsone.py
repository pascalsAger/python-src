""" This is sample doc """
temperatures=[10,-20,-289,100]

def convert_celsius(celcius):
	""" This doc belongs to convert celsius """
	fahrenheit = float(celcius) * (9/5) + 32
	return fahrenheit


with open("temp.txt","w+") as file:
	for celcius in temperatures:
		if float(celcius) > -273.15:
			file.write(str(convert_celsius(celcius))+"\n")

